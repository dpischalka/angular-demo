import {NgModule} from '@angular/core';

import {NgDestroyService} from './services/ng-destroy.service';

@NgModule({
  providers: [NgDestroyService],
})
export class SharedModule {
}
