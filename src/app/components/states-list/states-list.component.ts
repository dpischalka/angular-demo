import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {USStatesActions} from '@store/actions/us-states';
import {AppState} from '@store/state';
import {selectUSStates} from '@store/selectors';
import {Observable} from 'rxjs';
import {StateModel} from '@store/models';
import {filter, takeUntil} from 'rxjs/operators';
import {MatTableDataSource} from '@angular/material/table';
import {NgDestroyService} from '../../shared/services/ng-destroy.service';

interface USStatesCovidStructure {
  state: string;
  name: string;
  covid19Site: string;
  twitter: string;
}

@Component({
  selector: 'app-states-list',
  templateUrl: './states-list.component.html',
  styleUrls: ['./states-list.component.scss']
})
export class StatesListComponent implements OnInit {
  private USStates$: Observable<StateModel[]> = this.store$
    .select(selectUSStates)
    .pipe(
      filter((USStateTablesData: StateModel[]): boolean => Boolean(USStateTablesData.length)),
      takeUntil(this.ngOnDestroy$),
    );

  public dataSource = new MatTableDataSource<StateModel>();

  public paginationPageSizeOptions: number[] = [5, 10, 20];

  constructor(private store$: Store<AppState>,
              private ngOnDestroy$: NgDestroyService) {
  }

  ngOnInit(): void {
    this.USStates$
      .subscribe((USStates: StateModel[]) => {
        this.dataSource.data = USStates;
      });

    this.store$.dispatch(USStatesActions.getStates());
  }
}
